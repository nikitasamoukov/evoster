#pragma once

#include "CoreMinimal.h"
#include "Engine/DPICustomScalingRule.h"
#include "FullHdDPIScalingRule.generated.h"

UCLASS()
class EVOSTER_API UFullHdDPIScalingRule : public UDPICustomScalingRule
{
	GENERATED_BODY()

	virtual float GetDPIScaleBasedOnSize(FIntPoint Size) const override;
};
