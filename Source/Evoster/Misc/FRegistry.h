// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "FRegistry.generated.h"


USTRUCT(BlueprintType)
struct FEntity
{
	GENERATED_BODY()
public:
	using entity_type = entt::entt_traits<std::uint64_t>::entity_type;

	FEntity(entity_type Value = entt::null)
		: Val(Value) {}

	FEntity(const FEntity& Other)
		: Val(Other.Val) {}

	operator entity_type() const
	{
		return Val;
	}

protected:
	entity_type Val;
};

template <>
struct entt::entt_traits<FEntity> : entt::entt_traits<std::uint64_t> {};


UCLASS()
class URegistry : public UObject
#if CPP
                  , public entt::basic_registry<FEntity>
#endif
{
	GENERATED_BODY()
public:
	template <typename... Component>
	[[nodiscard]] bool has(const entity_type entity) const
	{
		return all_of<Component...>(entity);
	}
};

template <>
struct TIsPODType<URegistry>
{
	enum { Value = false };
};

template <>
struct TStructOpsTypeTraits<URegistry> : public TStructOpsTypeTraitsBase2<URegistry>
{
	enum
	{
		WithCopy = false,
	};
};

USTRUCT(BlueprintType)
struct FEnttHandle
#if CPP
	: public entt::basic_handle<FEntity>
#endif
{
	GENERATED_BODY()
public:
	FEnttHandle() {}
	FEnttHandle(URegistry& Reg, FEntity Entity) : entt::basic_handle<FEntity>(Reg, Entity) {}
	~FEnttHandle() {}

	URegistry* registry() const
	{
		return (URegistry*)entt::basic_handle<FEntity>::registry();
	}

	bool IsValid() const;

	template <typename... Component>
	[[nodiscard]] decltype(auto) has() const
	{
		return all_of<Component...>();
	}

	bool friend operator ==(FEnttHandle const& A, FEnttHandle const& B);
};

template <>
struct TStructOpsTypeTraits<FEnttHandle> : public TStructOpsTypeTraitsBase2<FEnttHandle>
{
	enum
	{
		WithIdenticalViaEquality = true,
	};
};
