#pragma once

#pragma warning (disable : 5054)
#pragma warning (disable : 5055)

#include "Core.h"
#include "UObject/StrongObjectPtr.h"
#include "Engine/World.h"
#include "Misc/TVariant.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/StaticMeshActor.h"
#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"

#include "entt.hpp"
#include "Misc/FRegistry.h"
#include "Evoster/Common/Common.h"
#include <variant>

inline void E_LOG_IMPL(const char* File, int Line, const char* Function, FString const& Text)
{
	UE_LOG(LogTemp, Warning, TEXT("%s (file:%s line:%i func:%s"), *Text, *FString(File), Line, *FString(Function));
}

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#define E_LOG(msg) (E_LOG_IMPL( __FILENAME__, __LINE__, __FUNCTION__, (msg)))
#define E_LOG_CRIT() (E_LOG("CRITICAL ERROR"))

#define PROFILER_SCOPE_FUNCTION() TRACE_CPUPROFILER_EVENT_SCOPE_STR(__FUNCTION__)
