#pragma once
#include "Containers/Array.h"
#include "Math/IntPoint.h"

template <typename T>
class TArray2d
{
public:
	[[nodiscard]]
	T& At(int X, int Y)
	{
		return Data[ConvIdx(Size, X, Y)];
	}
	
	[[nodiscard]]
	T const& At(int X, int Y) const
	{
		return Data[ConvIdx(Size, X, Y)];
	}

	[[nodiscard]]
	T& At(FIntPoint Pos)
	{
		return At(Pos.X, Pos.Y);
	}
	
	[[nodiscard]]
	T const& At(FIntPoint Pos) const
	{
		return At(Pos.X, Pos.Y);
	}
	
	[[nodiscard]]
	T& operator[](FIntPoint Pos)
	{
		return At(Pos);
	}
	
	[[nodiscard]]
	T const& operator[](FIntPoint Pos) const
	{
		return At(Pos);
	}

	void SetNum(int X, int Y)
	{
		SetNum({ X, Y });
	}

	void SetNum(FIntPoint NewSize)
	{
		if (NewSize == Size) return;
		// Not so effective
		TArray<T> NewData;
		NewData.SetNum(NewSize.X * NewSize.Y);

		for (int Y = 0; Y < FMath::Min(NewSize.Y, Size.Y); Y++)
			for (int X = 0; X < FMath::Min(NewSize.X, Size.X); X++)
			{
				NewData[ConvIdx(NewSize, X, Y)] = MoveTemp(Data[ConvIdx(Size, X, Y)]);
			}

		Data = MoveTemp(NewData);

		Size = NewSize;
	}

	void Fill(T const& Val)
	{
		for (auto& E : Data)
		{
			E = Val;
		}
	}

	FIntPoint Num() const
	{
		return Size;
	}
	
	[[nodiscard]]
	auto& GetData()
	{
		return Data;
	}
	
	[[nodiscard]]
	auto& GetData() const
	{
		return Data;
	}

	bool ValidPos(FIntPoint const& Pos) const
	{
		return (Pos.X >= 0) & (Pos.Y >= 0) & (Pos.X < Num().X) & (Pos.Y < Num().Y);
	}
	
	static int ConvIdx(FIntPoint Size, int X, int Y)
	{
		return X + Y * Size.X;
	}
private:

	TArray<T> Data;
	FIntPoint Size{ 0 };
};
