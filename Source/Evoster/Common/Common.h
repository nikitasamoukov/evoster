#pragma once

#include "Containers/UnrealString.h"
#include "UObject/ObjectMacros.h"
#include "LibExt/TArray2d.h"

template <typename C>
FString GetStringFromType()
{
	auto Name = entt::type_id<C>().name();
	return std::string(Name).c_str();
}

template <typename C>
FString GetStringFromTypeOmitClass()
{
	FString Str = GetStringFromType<C>();

	int StartIdx = 0;

	for (size_t i = 1; i < Str.Len(); i++)
	{
		if (Str[i - 1] == ' ')
		{
			StartIdx = i;
		}
	}
	Str.RemoveAt(0, StartIdx);

	return Str;
}

template <typename EnumType>
EnumType GetEnumValueFromString(const FString& String)
{
	FString Str = GetStringFromTypeOmitClass<EnumType>();

	UEnum* Enum = FindObject<UEnum>(ANY_PACKAGE, *Str, true);
	if (!Enum)
	{
		return EnumType(0);
	}
	return (EnumType)Enum->GetValueByName(FName(*String));
}

template <typename TEnum>
FString GetEnumValueAsString(TEnum Value)
{
	//const FString& Name
	FString Str = GetStringFromTypeOmitClass<TEnum>();

	const UEnum* Enum = FindObject<UEnum>(ANY_PACKAGE, *Str, true);
	if (!Enum) return FString("<Invalid>");
	return Enum->GetDisplayNameTextByValue((int64)Value).ToString();
}


using f32 = float;
using f64 = double;

using u8 = uint8_t;
using i8 = int8_t;

using u16 = uint16_t;
using i16 = int16_t;

using u32 = uint32_t;
using i32 = int32_t;

using u64 = uint64_t;
using i64 = int64_t;


inline void DEBUG_MESSAGE(const FString& Message, const float TimeToDisplay = 5.f,
	const FColor& MessageColor = FColor::Yellow)
{
	//if(GEngine)
	//    GEngine->AddOnScreenDebugMessage(-1, TimeToDisplay, MessageColor, Message);
}
