#pragma once

#include "FMapCell.generated.h"


USTRUCT(BlueprintType)
struct EVOSTER_API FCellEmpty
{
	GENERATED_BODY()
	bool friend operator ==(FCellEmpty const& A, FCellEmpty const& B) = default;
};

USTRUCT(BlueprintType)
struct EVOSTER_API FCellFood
{
	GENERATED_BODY()
	bool friend operator ==(FCellFood const& A, FCellFood const& B) = default;
};

USTRUCT(BlueprintType)
struct EVOSTER_API FCellWall
{
	GENERATED_BODY()
	bool friend operator ==(FCellWall const& A, FCellWall const& B) = default;
};

USTRUCT(BlueprintType)
struct EVOSTER_API FCellEntity
{
	GENERATED_BODY()

	FEntity Entity;
	bool friend operator ==(FCellEntity const& A, FCellEntity const& B) = default;
};

//template <class>
//inline constexpr bool TAlwaysFalseV = false;
//
//inline bool operator==(
//	TVariant<FCellEmpty, FCellFood, FCellWall, FCellEntity> const& A,
//	TVariant<FCellEmpty, FCellFood, FCellWall, FCellEntity> const& B)
//{
//	if (A.GetIndex() != B.GetIndex())
//		return false;
//
//	bool Res = true;
//	Visit([&]<typename T>(T const& Val)
//	{
//		if constexpr (
//			TIsSame<T, FCellEmpty>::Value ||
//			TIsSame<T, FCellFood>::Value ||
//			TIsSame<T, FCellWall>::Value ||
//			TIsSame<T, FCellEntity>::Value
//		)
//			Res = Val == B.Get<T>();
//		else
//			static_assert(TAlwaysFalseV<T>);
//	}, A);
//	return Res;
//}


UENUM(BlueprintType)
enum class EMapCellType:uint8
{
	Empty,
	Food,
	Wall,
	Entity
};

USTRUCT(BlueprintType)
struct EVOSTER_API FMapCell
{
	GENERATED_BODY()
public:
	FMapCell();

	explicit FMapCell(auto const& Type): Type(Type) { }

	//std::variant<FCellEmpty, FCellFood, FCellWall, FCellEntity> Type;

	FEntity Entity;
	EMapCellType Type = EMapCellType::Empty;

	bool friend operator ==(FMapCell const& A, FMapCell const& B) = default;
};

template <>
struct TStructOpsTypeTraits<FMapCell> : public TStructOpsTypeTraitsBase2<FMapCell>
{
	enum
	{
		WithIdenticalViaEquality = true,
		WithCopy = true,
	};
};

UCLASS()
class EVOSTER_API UMapCellLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static EMapCellType GetType(UPARAM(Ref) FMapCell& MapCell)
	{
		/*
		EMapCellType Res = EMapCellType::Empty;

		visit([&]<typename T>(T const& Val)
		{
			if constexpr (TIsSame<T, FCellEmpty>::Value)
				Res = EMapCellType::Empty;
			else if constexpr (TIsSame<T, FCellFood>::Value)
				Res = EMapCellType::Food;
			else if constexpr (TIsSame<T, FCellWall>::Value)
				Res = EMapCellType::Wall;
			else if constexpr (TIsSame<T, FCellEntity>::Value)
				Res = EMapCellType::Entity;
			else
				static_assert("???");
		}, MapCell.Type);*/

		return MapCell.Type;
	}

	UFUNCTION(BlueprintCallable)
	static void GetEntity(UPARAM(Ref) FMapCell& MapCell, FEntity& CellEntity)
	{
		CellEntity = MapCell.Entity;
		/*
		if (auto Val = get_if<FCellEntity>(&MapCell.Type))
		{
			CellEntity = *Val;
		}
		else
		{
			CellEntity = {};
		}*/
	}
};
