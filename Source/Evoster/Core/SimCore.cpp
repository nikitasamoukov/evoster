#include "SimCore.h"
#include "FGameMap.h"
#include "SimOptions.h"
#include "Components/CDna.h"
#include "Components/CreatureComponents.h"
#include "Helpers/HelpersCreature.h"
#include "Systems/SystemGrow.h"
#include "Systems/SystemLeafGrow.h"
#include "Systems/SystemLifetime.h"
#include "Systems/SystemSeeds.h"
#include "Systems/SystemSunLight.h"

USimCore::USimCore()
{
	Registry = CreateDefaultSubobject<URegistry>(TEXT("MMM"));
	Registry->set<FGameMap>();
	Registry->ctx<FGameMap>().Resize({1, 1});
	Registry->set<FRandomStream>();
}

FCDna GetGrass()
{
	FCDna CDna;

	CDna.LeafType = FCellType::FromName("leaf_basic");

	return CDna;
}

FCDna GetBush()
{
	FCDna CDna;

	CDna.LeafType = FCellType::FromName("leaf_basic");
	CDna.Genes.Push({{0, 0}, FGeneAdd{FCellType::FromName("branch_basic")}});

	return CDna;
}

FCDna GetBigBush()
{
	FCDna CDna;

	CDna.LeafType = FCellType::FromName("leaf_basic");
	CDna.Genes.Push({{0, 0}, FGeneAdd{FCellType::FromName("branch_basic")}});
	CDna.Genes.Push({{1, 0}, FGeneAdd{FCellType::FromName("branch_basic")}});
	CDna.Genes.Push({{-1, 0}, FGeneAdd{FCellType::FromName("branch_basic")}});
	CDna.Genes.Push({{0, 1}, FGeneAdd{FCellType::FromName("branch_basic")}});
	CDna.Genes.Push({{0, -1}, FGeneAdd{FCellType::FromName("branch_basic")}});

	return CDna;
}

void USimCore::Start(USimOptions* SimOptions)
{
	if (!SimOptions)
	{
		E_LOG("OOPS");
		return;
	}
	Registry->ctx<FGameMap>().Resize(SimOptions->SimPlaneSize);
	Registry->clear();
	Registry->ctx<FRandomStream>().Initialize("Test");


	auto& GameMap = Registry->ctx<FGameMap>();
	HelpersCreature::Add(*Registry, GetGrass(), { {GameMap.Map.Num().X / 4, GameMap.Map.Num().Y / 4} });
	HelpersCreature::Add(*Registry, GetGrass(), { {GameMap.Map.Num().X / 4*2, GameMap.Map.Num().Y / 4} });
	//AddCreature(*Registry, GetBush(), { GameMap.Map.Num().X * 3 / 4, GameMap.Map.Num().Y / 2 });
	//AddCreature(*Registry, GetBigBush(), { GameMap.Map.Num().X / 2, GameMap.Map.Num().Y / 2 });
}

TPair<TArray<FColor>*, FIntPoint> USimCore::GetColors()
{
	PROFILER_SCOPE_FUNCTION();

	//Registry->ctx<FGameMap>().FillColors(*Registry);

	return {&Registry->ctx<FGameMap>().Colors.GetData(), Registry->ctx<FGameMap>().Colors.Num()};
}

void SystemTest(URegistry& Registry)
{
	PROFILER_SCOPE_FUNCTION();
	auto& RandomStream = Registry.ctx<FRandomStream>();
	for (auto [Entity, CPos, CCreature] : Registry.view<FCPos, FCCreature>().each())
	{
		if (RandomStream.RandRange(0, 100) != 0) continue;
		auto NewPos = CPos.GetPos();
		NewPos.X += RandomStream.RandRange(-5, 5);
		NewPos.Y += RandomStream.RandRange(-5, 5);

		//AddCreature(Registry, CCreature, NewPos);
	}
}

void USimCore::Update()
{
	PROFILER_SCOPE_FUNCTION();
	auto& Map = Registry->ctx<FGameMap>().Map;
	auto Size = Map.Num();

	int Steps = 1;

	for (int i = 0; i < Steps; i++)
	{
		Check(*Registry);
		SystemLeafGrow(*Registry);
		Check(*Registry);
		SystemSunLight(*Registry);
		Check(*Registry);
		SystemGrow(*Registry);
		Check(*Registry);
		SystemSeeds(*Registry);
		Check(*Registry);
		SystemLifetime(*Registry);
		Check(*Registry);
	}
}

FMapCell USimCore::GetAt(FIntPoint const& Pos)
{
	return Registry->ctx<FGameMap>().Map.At(Pos);
}
