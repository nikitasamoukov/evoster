#include "FFactoryComponents.h"

struct FImGuiComponentDrawContext;
class LRegistryImGuiWindowImpl;

template <typename T>
concept CComponentSpecialImGuiDraw = requires(FImGuiComponentDrawContext const& Ctx, T const& C)
{
	{ ImGuiCDraw(Ctx, C) } -> std::same_as<void>;
};

void FFactoryComponents::AddType(entt::type_info Ti, FFactoryComponentsCCell&& CInfo)
{
	TypesMap.Add(Ti.seq(), CInfo);
	NameToTypeInfo.Add(*CInfo.Name, Ti);
}

FFactoryComponentsCCell const* FFactoryComponents::GetTypeCell(entt::id_type TiSeq)
{
	if (!TypesMap.Contains(TiSeq)) return nullptr;
	return &TypesMap[TiSeq];
}

FFactoryComponentsCCell const* FFactoryComponents::GetTypeCell(entt::type_info Ti)
{
	return GetTypeCell(Ti.seq());
}

FFactoryComponentsCCell const* FFactoryComponents::GetTypeCell(FName CompName)
{
	if (!NameToTypeInfo.Contains(CompName)) return nullptr;
	return GetTypeCell(NameToTypeInfo[CompName]);
}

template <typename C>
concept CHasXmlLoad = requires(C& Comp, FXmlNode* Node)
{
	LoadComponentData(Comp, Node);
};

template <typename C>
void RegComp(FFactoryComponents& Factory, FString const& Name)
{
	FFactoryComponentsCCell Cell;

	Cell.Name = Name;

	{
		//CompIsRef
		Cell.CompIsRef = CCompIsRef<C>;
	}
	{
		//FuncImguiDraw
		if constexpr (CComponentSpecialImGuiDraw<C>)
		{
			//Cell.FuncImguiDraw = [](FImGuiComponentDrawContext const& Ctx) {
			//	if constexpr (!std::is_empty_v<C>) {
			//		auto Comp = Ctx.Reg->try_get<C>(Ctx.Entity);
			//		if (Comp)
			//			ImGuiCDraw(Ctx, *Comp);
			//		else
			//			E_LOG_CRIT();
			//	}
			//	else
			//	{
			//		C Comp;
			//		ImGuiCDraw(Ctx, Comp);
			//	}
			//};
			if constexpr (!std::is_empty_v<C>)
			{
				Cell.FuncImguiDraw = [](FImGuiComponentDrawContext const& Ctx)
				{
					auto Comp = Ctx.Reg->try_get<C>(Ctx.Entity);
					if (Comp)
						ImGuiCDraw(Ctx, *Comp);
					else
						E_LOG_CRIT();
				};
			}
			else
			{
				Cell.FuncImguiDraw = [](FImGuiComponentDrawContext const& Ctx)
				{
					C Comp;
					ImGuiCDraw(Ctx, Comp);
				};
			}
		}
		else
		{
			if constexpr (CCompIsRef<C>)
			{
				Cell.FuncImguiDraw = [](FImGuiComponentDrawContext const& Ctx)
				{
					auto Comp = Ctx.Reg->try_get<C>(Ctx.Entity);
					if (Comp)
						ImGuiCDrawRef(Ctx, *Comp);
					else
						E_LOG_CRIT();
				};
			}
			else
			{
				Cell.FuncImguiDraw = [](FImGuiComponentDrawContext const& Ctx)
				{
					ImGuiCDrawNoReflectionAndDraw(Ctx, entt::type_id<C>());
				};
			}
		}
	}
	{
		//ImguiDrawPriority
		Cell.ImguiDrawPriority = ImGuiCDrawGetPri<C>();
	}

	{
		//FuncLoadXml

		if constexpr (CHasXmlLoad<C>)
		{
			Cell.FuncLoadXml = [](FEnttHandle Handle, FXmlNode* Node)
			{
				if (Handle.has<C>())
				{
					UE_LOG(LogTemp, Warning, TEXT("Component double load: %s"), *GetStringFromType<C>());
					return;
				}
				C Comp;
				LoadComponentData(Comp, Node);
				Handle.emplace<C>(Comp);
			};
		}
	}

	{
		//FuncHaveComp
		Cell.FuncHaveComp = [](URegistry const& Reg, FEntity Entity)
		{
			return Reg.has<C>(Entity);
		};
	}
	{
		//FuncCopy
		if constexpr (!std::is_empty_v<C>)
		{
			Cell.FuncCopy = [](FEnttHandle BreedHandle, FEnttHandle EntityHandle)
			{
				EntityHandle.emplace_or_replace<C>(BreedHandle.get<C>());
			};
		}
		else
		{
			Cell.FuncCopy = [](FEnttHandle BreedHandle, FEnttHandle EntityHandle)
			{
				EntityHandle.emplace<C>();
			};
		}
	}

	Factory.AddType(entt::type_id<C>(), MoveTemp(Cell));
}

#define REG_COMPONENT(C) RegComp<C>(*this, #C)

FFactoryComponents::FFactoryComponents()
{
	//REG_COMPONENT(FCBreedId);
}

#undef REG_COMPONENT
