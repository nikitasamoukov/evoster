#pragma once
#include "CreatureComponents.h"

#include "CDna.generated.h"

USTRUCT(BlueprintType)
struct FGeneAdd
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FCellType Type;
};

USTRUCT(BlueprintType)
struct FGeneChange
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FCellType Type;
};

USTRUCT(BlueprintType)
struct FGene
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FLocalPos Pos{0};

	std::variant<FGeneAdd, FGeneChange> Type;
};

UENUM(BlueprintType)
enum class EGeneVariantType:uint8
{
	Add,
	Change
};

UCLASS()
class UFGeneLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Meta = (ExpandEnumAsExecs = "GeneVariantType"))
	static void GetGeneVariantType(FGene const& Gene, EGeneVariantType& GeneVariantType)
	{
		GeneVariantType = (EGeneVariantType)Gene.Type.index();
	}

	UFUNCTION(BlueprintCallable)
	static FGeneAdd GetGeneVariantValAdd(FGene const& Gene)
	{
		auto Val = std::get_if<FGeneAdd>(&Gene.Type);
		if (Val) return *Val;
		return {};
	}

	UFUNCTION(BlueprintCallable)
	static FGeneChange GetGeneVariantValChange(FGene const& Gene)
	{
		auto Val = std::get_if<FGeneChange>(&Gene.Type);
		if (Val) return *Val;
		return {};
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (DisplayName = "MakeGene"))
	static FGene MakeGene1(FLocalPos const& Pos, FGeneAdd const& GeneAdd)
	{
		return {Pos, GeneAdd};
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (DisplayName = "MakeGene"))
	static FGene MakeGene2(FLocalPos const& Pos, FGeneChange const& GeneChange)
	{
		return {Pos, GeneChange};
	}
};

USTRUCT(BlueprintType)
struct FCDna
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	TArray<FGene> Genes;

	UPROPERTY(BlueprintReadWrite)
	FCellType LeafType;

	UPROPERTY(BlueprintReadWrite)
	int IdxGeneToExec = 0;

	FColor Color;

	FCDna Mutated(FRandomStream& RandomStream);
};
