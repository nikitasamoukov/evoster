#pragma once

#include "CDna.h"

TOptional<FCellType> RandomCellType(FRandomStream& RandomStream)
{
	for(int i=0;i<10;i++)
	{
		int Idx=RandomStream.RandRange(1, GCellTypesDesc.Num() - 1);
		if (GCellTypesDesc[Idx].IsType<FCellTypeLeaf>()) continue;
		if (GCellTypesDesc[Idx].IsType<std::monostate>()) continue;

		return FCellType{ (int16_t)Idx };
	}
	return {};
}

FCDna FCDna::Mutated(FRandomStream& RandomStream)
{
	FCDna CDna = *this;

	if (RandomStream.FRand() < 0.3)
	{
		FLocalPos Pos{0};
		if (CDna.Genes.Num() > 0)
		{
			int i = RandomStream.RandRange(0, CDna.Genes.Num() - 1);
			Pos = CDna.Genes[i].Pos;
			Pos += FCPos::SidePositionsOffsets[RandomStream.RandRange(0, 3)];
		}
		if (auto CellType = RandomCellType(RandomStream))
		{
			CDna.Genes.Add(FGene{Pos, FGeneAdd{*CellType}});
		}
	}
	//if (RandomStream.FRand() < 0.3)
	//{
	//}
	//if (RandomStream.FRand() < 0.3)
	//{
	//}

	return CDna;
}
