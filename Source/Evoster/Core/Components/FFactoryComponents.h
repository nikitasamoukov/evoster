#pragma once

struct FImGuiComponentDrawContext;
class FXmlNode;

struct FFactoryComponentsCCell 
{
	FString Name;

	TFunction<void(FImGuiComponentDrawContext const& Ctx)> FuncImguiDraw;
	float ImguiDrawPriority = 10000;
	bool CompIsRef = false;


	TFunction<void(FEnttHandle Handle, FXmlNode* Node)> FuncLoadXml;

	using FFuncHaveComp = TFunction<bool(URegistry const&, FEntity)>;
	using FFuncCopy = TFunction<void(FEnttHandle TemplateHandle, FEnttHandle EntityHandle)>;

	FFuncHaveComp FuncHaveComp;
	FFuncCopy FuncCopy;
};

struct FFactoryComponents
{
	FFactoryComponents();

	void AddType(entt::type_info Ti, FFactoryComponentsCCell&& CInfo);
	FFactoryComponentsCCell const* GetTypeCell(entt::id_type TiSeq);
	FFactoryComponentsCCell const* GetTypeCell(entt::type_info Ti);
	FFactoryComponentsCCell const* GetTypeCell(FName CompName);

	TMap<entt::id_type, FFactoryComponentsCCell> const& GetAll()
	{
		return TypesMap;
	}

protected:
	TMap<entt::id_type, FFactoryComponentsCCell> TypesMap;
	TMap<FName, entt::type_info> NameToTypeInfo;
};
