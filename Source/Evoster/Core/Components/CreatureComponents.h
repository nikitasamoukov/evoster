#pragma once

#include "CreatureComponents.generated.h"

struct FCDna;
struct FCCreature;

struct EVOSTER_API FCellTypeBranch
{
	int Height = 0;
	int Radius = 0;
};

struct EVOSTER_API FCellTypeLeaf
{
	float EnergyMul = 0;
};

struct EVOSTER_API FCellDataLeaf
{
	int Height = 0;
};

struct EVOSTER_API FCellTypeInfo
{
	FName Name;
	TSet<FName> Groups;

	FColor Color = FColor::White;

	int Cost = 0;
	int LifeAdd = 0;

	std::variant<std::monostate, FCellTypeBranch, FCellTypeLeaf> Info;

	template <class T>
	bool IsType() const { return std::holds_alternative<T>(Info); }

	template <class T>
	auto& AsType() const { return std::get<T>(Info); }
};

const inline TArray<FCellTypeInfo> GCellTypesDesc = []
{
	TArray<FCellTypeInfo> Res;

	auto AddType = [&](FCellTypeInfo Info)
	{
		Info.Groups.Add(Info.Name);
		Res.Add(Info);
	};

	AddType({
		"empty",
		{},
		FColor::FromHex("#FFF"),
		0,
		0,
		std::monostate()
	});

	AddType({
		"leaf_basic",
		{"group_leaf"},
		FColor::FromHex("#3F3"),
		4,
		40,
		FCellTypeLeaf{
			0.25f
		}
	});

	AddType({
		"branch_basic",
		{"group_branch"},
		FColor::FromHex("#9f6934"),
		70,
		100,
		FCellTypeBranch{
			2,
			1
		}
	});

	return Res;
}();

//const TArray<FColor> CreatureColors = []
//{
//	TArray<FColor> Arr;
//	AddAnyway(Arr, (int)ECellType::Empty, FColor::FromHex("#F0F"));
//	AddAnyway(Arr, (int)ECellType::Trunk, FColor::FromHex("#3f2a14"));
//	AddAnyway(Arr, (int)ECellType::Branch, FColor::FromHex("#9f6934"));
//	AddAnyway(Arr, (int)ECellType::Leaf, FColor::FromHex("3F3"));
//	AddAnyway(Arr, (int)ECellType::Spike, FColor::FromHex("#F33"));
//
//	return Arr;
//}();

USTRUCT(BlueprintType)
struct EVOSTER_API FCellType
{
	GENERATED_BODY()

	int16_t Idx = 0;
	FCellTypeInfo const& GetTypeInfo() const;

	static FCellType FromName(FName Name)
	{
		for (int i = 0; i < GCellTypesDesc.Num(); i++)
		{
			if (GCellTypesDesc[i].Name == Name)
				return FCellType{(int16_t)i};
		}

		E_LOG("OOPS");
		return {};
	}
};

FCellTypeInfo const& GetCellTypeInfo(FCellType Type);

// Global pos on map
USTRUCT(BlueprintType)
struct EVOSTER_API FGlobalPos : public FIntPoint
{
	GENERATED_BODY()
};

// Local pos of cell
USTRUCT(BlueprintType)
struct EVOSTER_API FLocalPos : public FIntPoint
{
	GENERATED_BODY()

	FLocalPos() : FIntPoint(0) {}
	FLocalPos(int32 InX, int32 InY) : FIntPoint(InX, InY) {}
	explicit FLocalPos(int32 InXY) : FIntPoint(InXY) {}
	explicit FLocalPos(FIntPoint const& A) : FIntPoint(A) {}

	FLocalPos operator+(const FIntPoint& Other) const
	{
		return FLocalPos(FIntPoint(*this) += Other);
	}
	FLocalPos operator-(const FIntPoint& Other) const { return FLocalPos(FIntPoint(*this) -= Other); }
};

USTRUCT(BlueprintType)
struct EVOSTER_API FCPos
{
	GENERATED_BODY()

	FCPos() = default;

	explicit FCPos(FIntPoint const& Pos) : Pos(Pos) { }

	static inline TArray<FIntPoint> SidePositionsOffsets = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

	FGlobalPos GetPos() const
	{
		return Pos;
	}

private:
	FGlobalPos Pos{0};
};

USTRUCT(BlueprintType)
struct EVOSTER_API FCell
{
	GENERATED_BODY()

	static FCell CreateLeaf(FCellType Type, FCellDataLeaf const& Data) { return {Type, Data}; }

	bool IsEmpty() const { return Type.Idx == 0; }

	struct FLeafInfo
	{
		int Height = -1;
		float MulEnergy = 1;
	};

	TOptional<FLeafInfo> GetLeafInfo(FCDna const& CDna) const;

	FCellType Type;
	std::variant<std::monostate, FCellDataLeaf> Data;
};

USTRUCT(BlueprintType)
struct EVOSTER_API FCCreature
{
	GENERATED_BODY()

	FCCreature();

	FCell const* GetCell(FLocalPos Pos) const;
	FCell const* GetCellGlobalPos(FCPos const& CPos, FGlobalPos Pos) const;

	static FGlobalPos ConvCellPosToGlobalPos(FGlobalPos CreaturePos, FLocalPos CellPos);
	static FLocalPos ConvGlobalPosToCellPos(FGlobalPos CreaturePos, FGlobalPos GlobalPos);

	bool HaveSideCell(FLocalPos const& CellPos);

	bool HaveCornerCell(FLocalPos const& CellPos);

	void ForEachCell(TFunction<void(FLocalPos const& Pos, FCell& Cell)> const& Func);

	int CalcMaxLifetime();
	FIntPoint Size() const;

	float Energy = 0;

	int FrameAge = 0;
private:
	// Grid offset. mainly negative values. Maybe need remove...
	FIntPoint Offset{0};
	TArray2d<FCell> Cells;

	FCell* GetCellMut(FLocalPos Pos);
	FCell* GetCellGlobalPosMut(FCPos const& CPos, FGlobalPos Pos);
	void SetCell(FLocalPos Pos, FCell Cell);

	friend class HelpersCreature;
};
