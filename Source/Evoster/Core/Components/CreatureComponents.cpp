#pragma once

#include "CreatureComponents.h"
#include "CDna.h"


FCellTypeInfo const& FCellType::GetTypeInfo() const
{
	return GetCellTypeInfo(*this);
}

FCellTypeInfo const& GetCellTypeInfo(FCellType Type)
{
	check(GCellTypesDesc.IsValidIndex(Type.Idx));

	return GCellTypesDesc[Type.Idx];
}

TOptional<FCell::FLeafInfo> FCell::GetLeafInfo(FCDna const& CDna) const
{
	auto& TI = Type.GetTypeInfo();
	if (TI.IsType<FCellTypeLeaf>())
	{
		return FLeafInfo{
			std::get<FCellDataLeaf>(Data).Height,
			CDna.LeafType.GetTypeInfo().AsType<FCellTypeLeaf>().EnergyMul
		};
	}
	if (TI.IsType<FCellTypeBranch>())
	{
		return FLeafInfo{
			TI.AsType<FCellTypeBranch>().Height,
			CDna.LeafType.GetTypeInfo().AsType<FCellTypeLeaf>().EnergyMul
		};
	}

	return {};
}

FCCreature::FCCreature() {}

FCell const* FCCreature::GetCell(FLocalPos Pos) const
{
	auto GridPos = Pos - Offset;

	if (!Cells.ValidPos(GridPos))
		return nullptr;
	auto& Cell = Cells[GridPos];
	if (Cell.IsEmpty())
		return nullptr;
	return &Cell;
}

FCell const* FCCreature::GetCellGlobalPos(FCPos const& CPos, FGlobalPos Pos) const
{
	auto LocalPos = ConvGlobalPosToCellPos(CPos.GetPos(), Pos);

	return GetCell(LocalPos);
}

FCell* FCCreature::GetCellMut(FLocalPos Pos)
{
	auto GridPos = Pos - Offset;

	if (!Cells.ValidPos(GridPos))
		return nullptr;
	auto& Cell = Cells[GridPos];
	if (Cell.IsEmpty())
		return nullptr;
	return &Cell;
}

FGlobalPos FCCreature::ConvCellPosToGlobalPos(FGlobalPos CreaturePos, FLocalPos CellPos)
{
	return {CellPos + CreaturePos};
}

FLocalPos FCCreature::ConvGlobalPosToCellPos(FGlobalPos CreaturePos, FGlobalPos GlobalPos)
{
	return FLocalPos{GlobalPos - CreaturePos};
}

FCell* FCCreature::GetCellGlobalPosMut(FCPos const& CPos, FGlobalPos Pos)
{
	auto LocalPos = ConvGlobalPosToCellPos(CPos.GetPos(), Pos);

	return GetCellMut(LocalPos);
}

void FCCreature::SetCell(FLocalPos Pos, FCell Cell)
{
	auto GridPos = Pos - Offset;

	FIntPoint MinPos(FMath::Min(GridPos.X, 0), FMath::Min(GridPos.Y, 0));
	FIntPoint MaxPos(FMath::Max(GridPos.X, Cells.Num().X - 1), FMath::Max(GridPos.Y, Cells.Num().Y - 1));

	FIntPoint OffsetOldInNew = FIntPoint(0) - MinPos;
	FIntPoint SizeNew = MaxPos - MinPos + FIntPoint(1);

	if (SizeNew != Cells.Num())
	{
		TArray2d<FCell> NewCells;
		NewCells.SetNum(SizeNew);

		for (int y = 0; y < Cells.Num().Y; y++)
			for (int x = 0; x < Cells.Num().X; x++)
			{
				NewCells[FIntPoint(x, y) + OffsetOldInNew] = Cells[{x, y}];
			}

		Cells = MoveTemp(NewCells);
		Offset += MinPos;

		GridPos = Pos - Offset;
	}

	Cells[GridPos] = Cell;
}

bool FCCreature::HaveSideCell(FLocalPos const& CellPos)
{
	if (GetCell(CellPos + FIntPoint(1, 0)))
		return true;
	if (GetCell(CellPos + FIntPoint(-1, 0)))
		return true;
	if (GetCell(CellPos + FIntPoint(0, 1)))
		return true;
	if (GetCell(CellPos + FIntPoint(0, -1)))
		return true;

	return false;
}

bool FCCreature::HaveCornerCell(FLocalPos const& CellPos)
{
	if (GetCell(CellPos + FIntPoint(1, 1)))
		return true;
	if (GetCell(CellPos + FIntPoint(-1, 1)))
		return true;
	if (GetCell(CellPos + FIntPoint(-1, -1)))
		return true;
	if (GetCell(CellPos + FIntPoint(1, -1)))
		return true;

	return false;
}

void FCCreature::ForEachCell(TFunction<void(FLocalPos const& Pos, FCell& Cell)> const& Func)
{
	auto Num = Cells.Num();
	auto& Data = Cells.GetData();
	for (int y = 0; y < Num.Y; y++)
		for (int x = 0; x < Num.X; x++)
		{
			auto& Cell = Data[Cells.ConvIdx(Num, x, y)];
			if (!Cell.IsEmpty())
				Func(FLocalPos(x, y) + Offset, Cell);
		}
}

int FCCreature::CalcMaxLifetime()
{
	int LifeTime = 0;
	ForEachCell([&](auto Pos, const FCell& Cell)
	{
		auto& TypeInfo = GetCellTypeInfo(Cell.Type);
		LifeTime += TypeInfo.LifeAdd;
	});
	return LifeTime;
}

FIntPoint FCCreature::Size() const { return Cells.Num(); }
