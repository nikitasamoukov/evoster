#include "HelpersCreature.h"
#include "Evoster/Core/FGameMap.h"
#include "Evoster/Core/Components/CDna.h"
#include "Evoster/Core/Components/CreatureComponents.h"

bool HelpersCreature::CanAddCopy(URegistry& Registry, FCCreature const& CCreature, FIntPoint const& NewCreaturePos)
{
	auto& GameMap = Registry.ctx<FGameMap>();

	for (int x = 0; x < CCreature.Cells.Num().X; x++)
		for (int y = 0; y < CCreature.Cells.Num().Y; y++)
		{
			FIntPoint Pos(x, y);
			Pos += NewCreaturePos + CCreature.Offset;
			if (!CCreature.Cells[{x, y}].IsEmpty())
				continue;
			if (!GameMap.Map.ValidPos(Pos))
				return false;
			if (GameMap.Map[Pos].Type != EMapCellType::Empty)
				return false;
		}
	return true;
}

bool HelpersCreature::CanAdd(URegistry& Registry, FIntPoint const& CreaturePos)
{
	auto& GameMap = Registry.ctx<FGameMap>();

	FIntPoint Pos = CreaturePos;
	if (GameMap.GetTypeAt(Pos) != EMapCellType::Empty)
		return false;

	return true;
}

FEnttHandle HelpersCreature::Add(URegistry& Registry, FCDna const& CDna, FGlobalPos const& CreaturePos)
{
	if (!CanAdd(Registry, CreaturePos)) { return {}; }

	FEnttHandle Handle(Registry, Registry.create());

	FCCreature CCreature;
	CCreature.SetCell(FLocalPos{0}, FCell::CreateLeaf(CDna.LeafType, {.Height = 1}));
	Handle.emplace_or_replace<FCCreature>(CCreature);
	Handle.emplace_or_replace<FCPos>(CreaturePos);
	Handle.emplace_or_replace<FCDna>(CDna);
	Handle.get<FCDna>().IdxGeneToExec = 0;

	FillCreature(Registry, Handle.entity(), CCreature, CreaturePos);
	return Handle;
}

void HelpersCreature::Remove(URegistry& Registry, FEntity Entity)
{
	auto [CCreature, CPos] = Registry.try_get<FCCreature, FCPos>(Entity);
	if (!CCreature || !CPos)
	{
		E_LOG_CRIT();
		return;
	}

	auto& GameMap = Registry.ctx<FGameMap>();
	Erase(Registry, Entity, *CCreature, CPos->GetPos());

	Registry.destroy(Entity);
}

bool HelpersCreature::CanSetCell(URegistry& Registry, FGlobalPos const& GlobalPos, FCell const& NewCell)
{
	auto& GameMap = Registry.ctx<FGameMap>();

	auto MapCell = GameMap.GetAt(GlobalPos);
	if (!MapCell)
		return false;
	if (MapCell->Type == EMapCellType::Empty)
		return true;
	if (MapCell->Type != EMapCellType::Entity)
		return false;

	auto [CCreature, CPos] = Registry.try_get<FCCreature, FCPos>(MapCell->Entity);
	if (!CCreature || !CPos)
	{
		E_LOG("OOPS");
		return false;
	}
	auto Cell = CCreature->GetCellGlobalPos(*CPos, GlobalPos);
	if (!Cell)
	{
		E_LOG("OOPS");
		return false;
	}

	if (NewCell.Type.GetTypeInfo().IsType<FCellTypeBranch>())
	{
		return Cell->Type.GetTypeInfo().IsType<FCellTypeLeaf>();
	}
	else if (NewCell.Type.GetTypeInfo().IsType<FCellTypeLeaf>())
	{
		if (Cell->Type.GetTypeInfo().IsType<FCellTypeBranch>())
			return false;
		if (Cell->Type.GetTypeInfo().IsType<FCellTypeLeaf>())
		{
			auto& Data = std::get<FCellDataLeaf>(Cell->Data);
			auto& NewCellData = std::get<FCellDataLeaf>(NewCell.Data);
			if (NewCellData.Height > Data.Height)
				return true;
			return false;
		}
	}
	E_LOG("OOPS");
	return false;
}

void HelpersCreature::CreatureRemoveCell(URegistry& Registry, FEntity const& Entity, FGlobalPos const& GlobalPos)
{
	auto [CCreature, CPos] = Registry.get<FCCreature, FCPos>(Entity);
	auto CellPos = CCreature.ConvGlobalPosToCellPos(CPos.GetPos(), GlobalPos);
	Erase(Registry, Entity, CCreature, CPos.GetPos());
	CCreature.Cells.At(CellPos).Type = FCellType{0};
	FillCreature(Registry, Entity, CCreature, CPos.GetPos());
	// TODO fill 1
}

bool HelpersCreature::SetCell(URegistry& Registry, FEntity const& Entity, FCCreature& CCreature, FCPos const& CPos,
	FLocalPos const& CellPos, FCell const& Cell)
{
	Check(Registry);

	auto GlobalPos = CCreature.ConvCellPosToGlobalPos(CPos.GetPos(), CellPos);

	if (!CanSetCell(Registry, GlobalPos, Cell))
		return false;

	auto& GameMap = Registry.ctx<FGameMap>();
	auto MapCell = GameMap.GetAt(GlobalPos);
	if (MapCell->Type == EMapCellType::Entity)
	{
		auto& CCreatureOther = Registry.get<FCCreature>(MapCell->Entity);
		auto& CPosOther = Registry.get<FCPos>(MapCell->Entity);
		auto CellOther = CCreatureOther.GetCellGlobalPosMut(CPosOther, GlobalPos);
		CellOther->Type = FCellType();
		CellOther->Data = std::monostate();
		MapCell->Type = EMapCellType::Empty;
	}

	Check(Registry);
	Erase(Registry, Entity, CCreature, CPos.GetPos());

	CCreature.SetCell(CellPos, Cell);

	//TODO fill 1 cell
	FillCreature(Registry, Entity, CCreature, CPos.GetPos());
	Check(Registry);
	return true;
}

void HelpersCreature::FillCreature(URegistry& Registry, FEntity const& Entity, FCCreature const& CCreature,
	FIntPoint const& CreaturePos)
{
	//E_LOG("Draw cells(" + FString::FromInt(Entity) + ")");
	auto& GameMap = Registry.ctx<FGameMap>();

	for (int x = 0; x < CCreature.Cells.Num().X; x++)
		for (int y = 0; y < CCreature.Cells.Num().Y; y++)
		{
			FGlobalPos Pos({x, y});
			Pos += CreaturePos + CCreature.Offset;
			auto CellType = CCreature.Cells[{x, y}];
			if (CellType.IsEmpty())
				continue;
			if (GameMap.Map.ValidPos(Pos))
			{
				//E_LOG("Draw cell("+ FString::FromInt (Entity) + "): " + FString::FromInt(Pos.X) + ", " + FString::
				//	FromInt(Pos.Y));
				auto& MapCell = GameMap.Map[Pos];
				if (MapCell.Type != EMapCellType::Empty)
				{
					auto Type = MapCell.Type;
					E_LOG_CRIT();
				}
				GameMap.Colors[Pos] = CellType.Type.GetTypeInfo().Color;
				MapCell.Type = EMapCellType::Entity;
				MapCell.Entity = Entity;
			}
		}
}

void HelpersCreature::Erase(URegistry& Registry, FEntity const& Entity, FCCreature& CCreature,
	FGlobalPos const& CreaturePos)
{
	//E_LOG("Erase cells(" + FString::FromInt(Entity) + ")");
	auto& GameMap = Registry.ctx<FGameMap>();
	CCreature.ForEachCell([&](FLocalPos Pos, const FCell& Cell)
	{
		auto GlobalPos = FCCreature::ConvCellPosToGlobalPos(CreaturePos, Pos);
		GameMap.Colors[GlobalPos] = GMapColors[(int)EMapCellType::Empty];
		GameMap.Map[GlobalPos].Type = EMapCellType::Empty;
		//E_LOG("Erase cell(" + FString::FromInt(Entity) + "): " + FString::FromInt(GlobalPos.X) + ", " +
		//	FString::FromInt(GlobalPos.Y));
	});
}

void Check(URegistry& Registry)
{
	return;
	PROFILER_SCOPE_FUNCTION();

	auto& GameMap = Registry.ctx<FGameMap>();

	//for (auto [Entity, CCreature, CPos] : Registry.view<FCCreature, FCPos>().each())
	//{
	//	int Count = 0;
	//	CCreature.ForEachCell([&](FIntPoint const& Pos, FCell& Cell)
	//	{
	//		Count++;
	//	});

	//	if (Count == 0)
	//	{
	//		E_LOG_CRIT();
	//		return;
	//	}
	//}

	for (int X = 0; X < GameMap.Map.Num().X; X++)
		for (int Y = 0; Y < GameMap.Map.Num().Y; Y++)
		{
			if (GameMap.Map[{X, Y}].Type == EMapCellType::Entity)
			{
				auto Entity = GameMap.Map[{X, Y}].Entity;
				if (!Registry.valid(Entity))
				{
					E_LOG_CRIT();
					return;
				}

				auto [CPos, CCreature] = Registry.get<FCPos, FCCreature>(Entity);

				auto Cell=CCreature.GetCellGlobalPos(CPos, FGlobalPos({X, Y}));

				if (!Cell)
				{
					E_LOG_CRIT();
					return;
				}
			}
		}

	for (auto [Entity, CCreature, CPos] : Registry.view<FCCreature, FCPos>().each())
	{
		CCreature.ForEachCell([&](FLocalPos const& Pos, FCell& Cell)
		{
			auto GlobalPos = CCreature.ConvCellPosToGlobalPos(CPos.GetPos(), Pos);
			auto MapCell = GameMap.GetAt(GlobalPos);
			if (!MapCell)
			{
				E_LOG_CRIT();
				return;
			}
			if (MapCell->Type != EMapCellType::Entity)
			{
				E_LOG_CRIT();
				return;
			}
			if (MapCell->Entity != Entity)
			{
				E_LOG_CRIT();
				return;
			}
		});
	}
}
