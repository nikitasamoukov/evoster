#pragma once
#include "Evoster/Core/Components/CreatureComponents.h"

struct FCell;
struct FCPos;
struct FCDna;
struct FCCreature;

class HelpersCreature
{
public:
	static bool CanAddCopy(URegistry& Registry, FCCreature const& CCreature, FIntPoint const& NewCreaturePos);

	static bool CanAdd(URegistry& Registry, FIntPoint const& CreaturePos);

	static FEnttHandle Add(URegistry& Registry, FCDna const& CDna, FGlobalPos const& CreaturePos);

	static void Remove(URegistry& Registry, FEntity Entity);

	// Return true if map pos free or lower leaf.
	static bool CanSetCell(
		URegistry& Registry,
		FGlobalPos const& GlobalPos,
		FCell const& NewCell);

	static void CreatureRemoveCell(
		URegistry& Registry,
		FEntity const& Entity,
		FGlobalPos const& GlobalPos);

	static bool SetCell(
		URegistry& Registry,
		FEntity const& Entity,
		FCCreature& CCreature,
		FCPos const& CPos,
		FLocalPos const& CellPos,
		FCell const& Cell);

private:
	static void FillCreature(URegistry& Registry, FEntity const& Entity, FCCreature const& CCreature,
		FIntPoint const& CreaturePos);

	static void Erase(URegistry& Registry, FEntity const& Entity, FCCreature& CCreature, FGlobalPos const& CreaturePos);
};


void Check(URegistry& Registry);
