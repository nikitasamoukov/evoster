#pragma once

#include "InstancedStruct.h"
#include "BreedRegistry.generated.h"

USTRUCT()
struct FEntityDesc
{
	GENERATED_BODY()
		
 	UPROPERTY(EditAnywhere, Category = Foo, meta = (BaseStruct = "TestStructBase"))
	FInstancedStruct Component;
};