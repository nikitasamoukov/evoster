#include "FGameMap.h"
#include "Components/CreatureComponents.h"

FGameMap::FGameMap()
{
	//Map.SetNum(10, 10);

	//for (int X = 0; X < Map.Num().X; X++)
	//	for (int Y = 0; Y < Map.Num().Y; Y++)
	//	{
	//		if (rand() % 2 == 0)
	//		{
	//			Map[{ X, Y }].Type = EMapCellType::Empty;
	//		}
	//		else
	//		{
	//			Map[{ X, Y }].Type = EMapCellType::Food;
	//		}
	//		Map[{ X, Y }].Type = EMapCellType::Empty;
	//	}
}

template <class T>
void AddAnyway(TArray<T>& Arr, int Idx, T const& E)
{
	check(Idx>=0);
	Arr.SetNum(FMath::Max(Arr.Num(), Idx + 1));
	Arr[Idx] = E;
}

const TArray<FColor> GMapColors = []
{
	TArray<FColor> Arr;
	AddAnyway(Arr, (int)EMapCellType::Empty, FColor::FromHex("#000"));
	AddAnyway(Arr, (int)EMapCellType::Food, FColor::FromHex("#AA0"));
	AddAnyway(Arr, (int)EMapCellType::Wall, FColor::FromHex("#AAA"));

	return Arr;
}();

void FGameMap::FillColors(URegistry& Registry)
{
	auto Size = Map.Num();

	Colors.SetNum(Map.Num());

	auto& Data = Colors.GetData();

	auto& RawMap = Map.GetData();
	auto Num = RawMap.Num();
	for (int i = 0; i < Num; i++)
	{
		switch (RawMap[i].Type)
		{
		case EMapCellType::Empty:
			Data[i] = GMapColors[(int)EMapCellType::Empty];
			break;
		case EMapCellType::Food:
			Data[i] = GMapColors[(int)EMapCellType::Food];
			break;
		case EMapCellType::Wall:
			Data[i] = GMapColors[(int)EMapCellType::Wall];
			break;
		case EMapCellType::Entity:
			{
				FEntity Entity = RawMap[i].Entity;
				auto CCreature = Registry.try_get<FCCreature>(Entity);
				auto CPos = Registry.try_get<FCPos>(Entity);
				if (!CCreature || !CPos)
				{
					goto default1;
				}
				auto CellType = CCreature->GetCellGlobalPos(*CPos, FGlobalPos{ { i % Size.X, i / Size.X } });
				if(!CellType)
				{
					goto default1;
				}
				Data[i] = CellType->Type.GetTypeInfo().Color;
				break;
			}
		default:
		default1:
			Data[i] = FColor(rand(), rand(), rand(), 255);
			break;
		}
		/*
		visit([&]<typename T>(T const& E)
		{
			if constexpr (TIsSame<T, FCellEmpty>::Value)
				Data[i] = FColor(10, 10, 10, 255);
			else if constexpr (TIsSame<T, FCellFood>::Value)
				Data[i] = FColor(0, 180, 0, 255);
			else if constexpr (TIsSame<T, FCellWall>::Value)
			{
				Data[i] = FColor(rand(), rand(), rand(), 255);
			}
			else if constexpr (TIsSame<T, FCellEntity>::Value)
			{
				Data[i] = FColor(rand(), rand(), rand(), 255);
			}
			else
				static_assert("???");
		}, RawMap[i].Type);
		*/
	}

	//for (int i = 0; i < Num; i++)
	//{
	//	Data[i] = FColor(i * 255 / Num, i * 255 / Num, i * 255 / Num, 255);
	//}
}

void FGameMap::Resize(FIntPoint const& Size)
{
	Map.SetNum(Size);
	Map.Fill(FMapCell(EMapCellType::Empty));

	FillColors(*(URegistry*)nullptr);
}

FMapCell* FGameMap::GetAt(FIntPoint const& Pos)
{
	if (!Map.ValidPos(Pos)) return nullptr;

	return &Map[Pos];
}

EMapCellType FGameMap::GetTypeAt(FIntPoint const& Pos)
{
	auto Cell = GetAt(Pos);
	if (!Cell) return EMapCellType::Wall;

	return Cell->Type;
}
