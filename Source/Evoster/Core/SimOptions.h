#pragma once

#include "SimOptions.generated.h"

UCLASS(BlueprintType)
class USimOptions : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite)
	FIntPoint SimPlaneSize = { 500 };
};
