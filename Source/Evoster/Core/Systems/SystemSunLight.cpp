#include "SystemSunLight.h"
#include "Evoster/Core/FGameMap.h"
#include "Evoster/Core/Components/CDna.h"
#include "Evoster/Core/Components/CreatureComponents.h"

void SystemSunLight(URegistry& Registry)
{
	PROFILER_SCOPE_FUNCTION();
	auto& RandomStream = Registry.ctx<FRandomStream>();
	auto& GameMap = Registry.ctx<FGameMap>();

	int GridSize = 3;
	FIntPoint GridOffset = {RandomStream.RandRange(0, GridSize - 1), RandomStream.RandRange(0, GridSize - 1)};

	int Samples = 3;

	for (int x = GridOffset.X - GridSize; x < GameMap.Map.Num().X; x += GridSize)
		for (int y = GridOffset.Y - GridSize; y < GameMap.Map.Num().Y; y += GridSize)
		{
			FCCreature* BestCCreature = nullptr;
			int BestHeight = -1;

			for (int i = 0; i < Samples; i++)
			{
				FGlobalPos SamplePos = {{ x, y }};
				FIntPoint OffsetInGrid(
					RandomStream.RandRange(0, GridSize - 1),
					RandomStream.RandRange(0, GridSize - 1)
				);
				SamplePos += OffsetInGrid;
				auto MapCell = GameMap.GetAt(SamplePos);
				if (!MapCell || MapCell->Type != EMapCellType::Entity)
					continue;
				auto CCreature = Registry.try_get<FCCreature>(MapCell->Entity);
				auto CPos = Registry.try_get<FCPos>(MapCell->Entity);
				auto CDna = Registry.try_get<FCDna>(MapCell->Entity);
				if (!CCreature || !CPos || !CDna)
				{
					E_LOG_CRIT();
					continue;
				}
				auto Cell = CCreature->GetCellGlobalPos(*CPos, SamplePos);
				if (!Cell)
				{
					E_LOG_CRIT();
					continue;
				}
				auto LeafInfo = Cell->GetLeafInfo(*CDna);
				if (!LeafInfo)
				{
					E_LOG_CRIT();
					continue;
				}
				if (LeafInfo->Height > BestHeight)
				{
					BestCCreature = CCreature;
					BestHeight = LeafInfo->Height;
				}
			}
			if (BestCCreature) {
				BestCCreature->Energy += GridSize * GridSize;
			}
		}
	/*for (auto [Entity, CPos, CCreature] : Registry.view<FCPos, FCCreature>().each())
	{
		TArray<FIntPoint> Leafs;
		CCreature.ForEachCell([&](auto Pos, FCell const& Cell)
		{
			auto& TypeInfo = GetCellTypeInfo(Cell.Type);
			if (TypeInfo.IsType<FCellTypeLeaf>() ||
				TypeInfo.IsType<FCellTypeBranch>())
			{
				Leafs.Add(Pos);
			}
		});

		if (RandomStream.RandRange(0, 5) <= Leafs.Num())
			CCreature.Energy++;
	}*/
}
