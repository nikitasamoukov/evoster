#include "SystemLeafGrow.h"
#include "Evoster/Core/Components/CDna.h"
#include "Evoster/Core/Components/CreatureComponents.h"
#include "Evoster/Core/Helpers/HelpersCreature.h"

void SystemLeafGrow(URegistry& Registry)
{
	PROFILER_SCOPE_FUNCTION();
	auto& RandomStream = Registry.ctx<FRandomStream>();

	for (auto [Entity, CPos, CCreature,CDna] : Registry.view<FCPos, FCCreature, FCDna>().each())
	{
		auto LeafCost = GetCellTypeInfo(CDna.LeafType).Cost;
		if (CCreature.Energy < LeafCost) continue;

		struct FBranchPointInfo
		{
			FLocalPos Pos{0};
			FCellTypeBranch BranchInfo;
		};

		TArray<FBranchPointInfo> BranchPoints;
		CCreature.ForEachCell([&](auto Pos, const FCell& Cell)
		{
			auto& TypeInfo = GetCellTypeInfo(Cell.Type);
			if (TypeInfo.IsType<FCellTypeBranch>())
			{
				BranchPoints.Add({Pos, TypeInfo.AsType<FCellTypeBranch>()});
			}
		});

		struct FLeafPointInfo
		{
			int Height = 0;
		};

		TMap<FIntPoint, FLeafPointInfo> LeafNeedPoints;

		for (auto& Info : BranchPoints)
		{
			auto Radius = Info.BranchInfo.Radius;
			for (int x = -Radius; x <= Radius; x++)
				for (int y = -Radius; y <= Radius; y++)
				{
					auto CellPos = Info.Pos + FIntPoint(x, y);
					auto CellAt = CCreature.GetCell(CellPos);
					if (!CellAt)
					{
						if (LeafNeedPoints.Contains(CellPos))
						{
							LeafNeedPoints[CellPos].Height = FMath::Max(LeafNeedPoints[CellPos].Height,
								Info.BranchInfo.Height);
						}
						else
						{
							LeafNeedPoints.Add(CellPos, {Info.BranchInfo.Height});
						}
					}
				}
		}

		if (LeafNeedPoints.Num() == 0) continue;
		TArray<TPair<FLocalPos, FLeafPointInfo>> LeafNeedPointsArray;
		for (auto& Pair : LeafNeedPoints)
		{
			LeafNeedPointsArray.Add(Pair);
		}
		auto NewLeafPair = LeafNeedPointsArray[RandomStream.RandRange(0, LeafNeedPoints.Num() - 1)];

		FCell Cell{.Type = CDna.LeafType, .Data = FCellDataLeaf{NewLeafPair.Value.Height}};

		bool Res = HelpersCreature::SetCell(Registry, Entity, CCreature, CPos, NewLeafPair.Key, Cell);

		if (Res)
		{
			CCreature.Energy -= LeafCost;
		}
	}
}
