#include "SystemGrow.h"
#include "Evoster/Core/Components/CDna.h"
#include "Evoster/Core/Components/CreatureComponents.h"
#include "Evoster/Core/Helpers/HelpersCreature.h"

enum class EGeneExecResult
{
	Success,
	Fail,
	Error
};

EGeneExecResult ExecGeneAdd(
	URegistry& Registry,
	FEntity const& Entity,
	FCPos const& CPos,
	FCCreature& CCreature,
	FGene const& Gene)
{
	auto GeneAdd = std::get<FGeneAdd>(Gene.Type);

	if (CCreature.Energy < GeneAdd.Type.GetTypeInfo().Cost)
		return EGeneExecResult::Fail;

	auto Cell = CCreature.GetCell(Gene.Pos);
	if (Cell && !Cell->Type.GetTypeInfo().IsType<FCellTypeLeaf>())
		return EGeneExecResult::Error;
	if (CCreature.Size() == FIntPoint::ZeroValue)
		return EGeneExecResult::Error;
	if (!CCreature.HaveSideCell(Gene.Pos) && CCreature.Size() != FIntPoint(1))
		return EGeneExecResult::Error;

	FCell NewCell;
	NewCell.Type = GeneAdd.Type;
	if (GeneAdd.Type.GetTypeInfo().IsType<FCellTypeLeaf>())
	{
		return EGeneExecResult::Error;
	}
	NewCell.Data = std::monostate();
	bool Res = HelpersCreature::SetCell(Registry, Entity, CCreature, CPos, Gene.Pos, NewCell);
	return Res ? EGeneExecResult::Success : EGeneExecResult::Fail;
}

void SystemGrow(URegistry& Registry)
{
	PROFILER_SCOPE_FUNCTION();
	auto& RandomStream = Registry.ctx<FRandomStream>();

	for (auto [Entity, CPos, CCreature, CDna] : Registry.view<FCPos, FCCreature, FCDna>().each())
	{
		if (!CDna.Genes.IsValidIndex(CDna.IdxGeneToExec))
			continue;

		auto& Gene = CDna.Genes[CDna.IdxGeneToExec];

		EGeneExecResult GeneExecResult = EGeneExecResult::Fail;

		visit([&]<class T>(T& GeneType)
			{
				if constexpr (std::is_same_v<FGeneAdd, T>)
				{
					GeneExecResult = ExecGeneAdd(Registry, Entity, CPos, CCreature, Gene);
				}
				else if constexpr (std::is_same_v<FGeneChange, T>)
				{
				}
				else
				{
					static_assert(std::is_same_v<void, T>);
				}
			},
			Gene.Type);

		if (GeneExecResult != EGeneExecResult::Fail)
		{
			CDna.IdxGeneToExec++;
		}
	}
}
