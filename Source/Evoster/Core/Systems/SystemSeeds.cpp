#include "SystemSeeds.h"
#include "Evoster/Core/Components/CDna.h"
#include "Evoster/Core/Components/CreatureComponents.h"
#include "Evoster/Core/Helpers/HelpersCreature.h"

void SystemSeeds(URegistry& Registry)
{
	PROFILER_SCOPE_FUNCTION();
	auto& RandomStream = Registry.ctx<FRandomStream>();

	for (auto [Entity, CPos, CCreature, CDna] : Registry.view<FCPos, FCCreature, FCDna>().each())
	{
		if (CDna.IdxGeneToExec < CDna.Genes.Num())
			continue;

		float SeedCost = CDna.LeafType.GetTypeInfo().Cost * 2;

		if (CCreature.Energy < SeedCost)
			continue;

		CCreature.Energy -= SeedCost;

		int Inc = CCreature.Size().GetMax() + 1;
		FIntPoint MinSeedPos = CCreature.ConvCellPosToGlobalPos(CPos.GetPos(), {0, 0});
		FIntPoint MaxSeedPos = CCreature.ConvCellPosToGlobalPos(CPos.GetPos(),
			FLocalPos{CCreature.Size() - FIntPoint{1, 1}});

		MinSeedPos -= Inc;
		MaxSeedPos += Inc;

		FGlobalPos Pos{
			{
				RandomStream.RandRange(MinSeedPos.X, MaxSeedPos.X),
				RandomStream.RandRange(MinSeedPos.Y, MaxSeedPos.Y)
			}
		};

		if (RandomStream.FRand() < 0.02)
		{
			HelpersCreature::Add(Registry, CDna.Mutated(RandomStream), Pos);
		}
		else
		{
			HelpersCreature::Add(Registry, CDna, Pos);
		}
	}
}
