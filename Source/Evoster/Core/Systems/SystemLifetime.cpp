#include "SystemLifetime.h"
#include "Evoster/Core/Components/CDna.h"
#include "Evoster/Core/Components/CreatureComponents.h"
#include "Evoster/Core/Helpers/HelpersCreature.h"

void SystemLifetime(URegistry& Registry)
{
	PROFILER_SCOPE_FUNCTION();
	for (auto [Entity, CCreature] : Registry.view<FCCreature>().each())
	{
		CCreature.FrameAge++;
		if(CCreature.FrameAge>CCreature.CalcMaxLifetime())
		{
			HelpersCreature::Remove(Registry, Entity);
		}
	}
}
