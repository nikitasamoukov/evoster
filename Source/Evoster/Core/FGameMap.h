#pragma once
#include "FMapCell.h"

#include "FGameMap.generated.h"

USTRUCT(BlueprintType)
struct EVOSTER_API FGameMap
{
	GENERATED_BODY()
public:
	FGameMap();

	void FillColors(URegistry& Registry);
	void Resize(FIntPoint const& Size);
	
	FMapCell* GetAt(FIntPoint const& Pos);
	EMapCellType GetTypeAt(FIntPoint const& Pos);
	
	TArray2d<FMapCell> Map;
	TArray2d<FColor> Colors;
};

const extern TArray<FColor> GMapColors;
