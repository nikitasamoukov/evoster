#pragma once

#include "CoreMinimal.h"
#include "FMapCell.h"

#include "SimCore.generated.h"

class USimOptions;

UCLASS(BlueprintType)
class EVOSTER_API USimCore : public UObject
{
	GENERATED_BODY()
public:
	USimCore();

	UFUNCTION(BlueprintCallable)
	void Start(USimOptions *SimOptions);
	TPair<TArray<FColor>*, FIntPoint> GetColors();
	void Update();
	
	UFUNCTION(BlueprintCallable)
	FMapCell GetAt(FIntPoint const& Pos);

	UPROPERTY()
	URegistry* Registry;
private:
	TPair<TArray<FColor>, FIntPoint> ColorsData;
};
