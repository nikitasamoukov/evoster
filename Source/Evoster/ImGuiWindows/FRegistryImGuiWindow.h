#pragma once

class LRegistryImGuiWindowImpl;

struct FImGuiComponentDrawContext
{
	LRegistryImGuiWindowImpl* RegWindow;
	URegistry const* Reg;
	FEntity Entity;
	int StackLevel;

	void ChildDraw(FEntity E, FString const& Name) const;
};

class URegistryImGuiWindow
{
public:
	URegistryImGuiWindow();
	~URegistryImGuiWindow();

	void DrawRegistry(URegistry const& Reg);

	void DrawEntity(URegistry const& Reg, FEntity Entity)
	{
		DrawEntity({ Impl.Get(), &Reg, Entity, 0 });
	}

	void DrawEntity(FImGuiComponentDrawContext const& Ctx);
protected:
	TUniquePtr<LRegistryImGuiWindowImpl> Impl;
};
