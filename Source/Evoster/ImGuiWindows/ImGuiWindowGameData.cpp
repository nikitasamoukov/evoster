// Fill out your copyright notice in the Description page of Project Settings.

#include "FRegistryImGuiWindow.h"
#include "imgui.h"

#include "Evoster/Core/SimCore.h"

class LImGuiDrawerMapKeys
{
public:
	TStaticArray<char, 256> Filter{};

	template <typename TVal>
	void Draw(TMap<FName, TVal> Map)
	{
		ImGui::InputText("##Filter", Filter.GetData(), Filter.Num());

		for (auto& Pair : Map)
			if (Pair.Key.ToString().Contains(Filter.GetData()) || Filter[0] == '\0')
			{
				ImGui::Text(TCHAR_TO_UTF8(*Pair.Key.ToString()));
			}
	}
};

class LImGuiDrawerStringsMap
{
public:
	TStaticArray<char, 256> Filter{};

	void Draw(TMap<FName, FString> Map)
	{
		ImGui::InputText("##Filter", Filter.GetData(), Filter.Num());

		for (auto& Pair : Map)
			if (Pair.Key.ToString().Contains(Filter.GetData()) || Filter[0] == '\0')
			{
				ImGui::Text("%s : %s", TCHAR_TO_UTF8(*Pair.Key.ToString()), TCHAR_TO_UTF8(*Pair.Value));
			}
	}
};


void ImGuiWindowGameData(USimCore& GameCore)
{
	auto& Registry = GameCore.Registry;

	ImGui::Begin("Game data");
	if (ImGui::BeginTabBar("##Tabs", ImGuiTabBarFlags_None))
	{
		if (ImGui::BeginTabItem("Templates"))
		{
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Textures"))
		{
			static LImGuiDrawerMapKeys Drawer;
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Strings"))
		{
			static LImGuiDrawerStringsMap Drawer;
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("T Raw"))
		{
			static URegistryImGuiWindow Drawer;
			Drawer.DrawRegistry(*Registry);
			ImGui::EndTabItem();
		}
		ImGui::EndTabBar();
	}

	ImGui::End();
}
