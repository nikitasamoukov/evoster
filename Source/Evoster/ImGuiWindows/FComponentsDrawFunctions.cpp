#include "FComponentsDrawFunctions.h"
#include <string>

#include "FRegistryImGuiWindow.h"
#include "imgui.h"

using std::function;
using std::string;
using std::to_string;
using std::vector;
using std::unordered_map;


FString ToFString(int Val)
{
	return FString::FromInt(Val);
}

FString ToFString(float Val)
{
	return FString::SanitizeFloat(Val);
}

FString ToFString(FColor Val)
{
	return Val.ToHex();
}

FString ToFString(FName Val)
{
	return Val.ToString();
}


#define IMGUI_DRAW_C_FIELD(field) ImGui::Text(#field": %ls", *ToFString(Component.field));


void ImGuiCDrawNoReflectionAndDraw(FImGuiComponentDrawContext const& Ctx, entt::type_info Ti)
{
	ImGui::Text("Component no reflection and draw: %s", std::string(Ti.name()).c_str());
}


void ImGuiCDrawPropImpl(FImGuiComponentDrawContext const& Ctx, FProperty* PropBase, void const* PropPtr, int Depth,
	TOptional<FString> VarName = {})
{
	if (Depth > 10) return;

	FString Spaces;
	for (int i = 0; i < Depth; i++)Spaces += "  ";
	FString SimpleVarName = "##4EC9B0FF" + PropBase->GetCPPType() + " ##9CDCFEFF" + PropBase->GetNameCPP() +
		"##FFFFFFFF";
	if (VarName) SimpleVarName = *VarName;
	FString VarVal;
	bool IsSimpleVal = false;

	// Values simple
	if (auto Prop = CastField<FFloatProperty>(PropBase))
	{
		float Val = *(float const*)PropPtr;
		VarVal = FString::SanitizeFloat(Val);
		IsSimpleVal = true;
	}
	if (auto Prop = CastField<FIntProperty>(PropBase))
	{
		int Val = *(int const*)PropPtr;
		VarVal = FString::FromInt(Val);
		IsSimpleVal = true;
	}
	if (auto Prop = CastField<FNameProperty>(PropBase))
	{
		FName Val = *(FName const*)PropPtr;
		VarVal = Val.ToString();
		IsSimpleVal = true;
	}
	if (PropBase->GetCPPType() == "FVector")
	{
		FVector Val = *(FVector const*)PropPtr;
		VarVal = FString::Printf(TEXT("(%3.3f, %3.3f, %3.3f)"), Val.X, Val.Y, Val.Z);
		IsSimpleVal = true;
	}

	if (IsSimpleVal)
	{
		ImGui::Text("%ls%ls:%ls", *Spaces, *SimpleVarName, *VarVal);
		return;
	}


	//Values special
	if (PropBase->GetCPPType() == "FEntity")
	{
		auto E = *(FEntity const*)PropPtr;

		Ctx.ChildDraw(E, "Entity:" + FString::FromInt((int)E));
		return;
	}

	if (auto Prop = CastField<FStructProperty>(PropBase))
	{
		UScriptStruct* NRef = Prop->Struct;
		ImGui::Text("%ls%ls:", *Spaces, *SimpleVarName);
		ImGuiCDrawRefImpl(Ctx, NRef, PropPtr, Depth + 1);
		return;
	}

	if (auto Prop = CastField<FArrayProperty>(PropBase))
	{
		FProperty* TemplateProperty = Prop->Inner;

		FString TypeName = "##4EC9B0FF" + PropBase->GetCPPType() + "##FFFFFFFF";
		FString TypeTemplateName = "##4EC9B0FF" + Prop->Inner->GetCPPType() + "##FFFFFFFF";
		FString VariableName = "##9CDCFEFF" + PropBase->GetNameCPP() + "##FFFFFFFF";
		//" ##9CDCFEFF" + PropBase->GetNameCPP();
		ImGui::Text("%ls%ls<%ls> %ls:", *Spaces, *TypeName, *TypeTemplateName, *VariableName);

		FScriptArrayHelper ArrayHelper(Prop, PropPtr);

		int32_t arrayValueCount = ArrayHelper.Num();
		for (size_t i = 0; i < arrayValueCount; i++)
		{
			uint8* ValuePtr = ArrayHelper.GetRawPtr(i);
			ImGuiCDrawPropImpl(Ctx, TemplateProperty, ValuePtr, Depth + 1, "[" + FString::FromInt(i) + "]");
		}
		return;
	}

	if (auto Prop = CastField<FMapProperty>(PropBase))
	{
		FProperty* TemplateKeyProperty = Prop->KeyProp;
		FProperty* TemplateValProperty = Prop->ValueProp;

		FString TypeName = "##4EC9B0FF" + PropBase->GetCPPType() + "##FFFFFFFF";
		FString TypeTemplateKeyName = "##4EC9B0FF" + TemplateKeyProperty->GetCPPType() + "##FFFFFFFF";
		FString TypeTemplateValueName = "##4EC9B0FF" + TemplateValProperty->GetCPPType() + "##FFFFFFFF";
		FString VariableName = "##9CDCFEFF" + PropBase->GetNameCPP() + "##FFFFFFFF";
		//" ##9CDCFEFF" + PropBase->GetNameCPP();
		ImGui::Text("%ls%ls<%ls, %ls> %ls:", *Spaces, *TypeName, *TypeTemplateKeyName, *TypeTemplateValueName,
		            *VariableName);

		FScriptMapHelper MapHelper(Prop, PropPtr);

		int32 ItemsLeft = MapHelper.Num();
		for (int32 Index = 0; ItemsLeft > 0; ++Index)
		{
			if (MapHelper.IsValidIndex(Index))
			{
				--ItemsLeft;

				uint8* Data = MapHelper.GetPairPtr(Index);

				ImGuiCDrawPropImpl(Ctx, TemplateKeyProperty, TemplateKeyProperty->ContainerPtrToValuePtr<uint8>(Data),
				                   Depth + 1);
				ImGuiCDrawPropImpl(Ctx, TemplateValProperty, TemplateValProperty->ContainerPtrToValuePtr<uint8>(Data),
				                   Depth + 1);
			}
			else
			{
				E_LOG_CRIT();
			}
		}
	}
}

void ImGuiCDrawRefImpl(FImGuiComponentDrawContext const& Ctx, UScriptStruct* Ref, void const* Component, int Depth)
{
	if (Depth > 10) return;

	for (TFieldIterator<FProperty> It(Ref); It; ++It)
	{
		FString Spaces;
		for (int i = 0; i < Depth; i++)Spaces += "  ";
		FString VarName = It->GetCPPType() + " " + It->GetNameCPP();
		FString VarVal;

		void const* NStr = It->ContainerPtrToValuePtr<void>(Component);
		ImGuiCDrawPropImpl(Ctx, *It, NStr, Depth);
	}
}


//void ImGuiCDraw(FImGuiComponentDrawContext const& Ctx, FCPrStar const& Component)
//{
//	ImGui::Text("%s %ls", "UE Projection", ToCStr(Component.Actor->GetName()));
//}


TMap<entt::id_type, float> GenPriMap()
{
	TArray<entt::type_info> Arr = {
		//{entt::type_id<FCMesh>()},
	};

	TMap<entt::id_type, float> Map;

	float Pri = 1;
	for (auto Ti : Arr)
	{
		Map.Add(Ti.seq(), Pri);
		Pri++;
	}
	return Map;
}

float ImGuiCDrawGetPri(entt::type_info Ti)
{
	static TMap<entt::id_type, float> PriMap = GenPriMap();

	if (PriMap.Contains(Ti.seq()))
	{
		return PriMap[Ti.seq()];
	}
	return 10000 + Ti.seq() * 0.1;
}
