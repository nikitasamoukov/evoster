// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EvosterGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class EVOSTER_API AEvosterGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
};
