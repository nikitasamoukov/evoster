#include "GameMapPlane.h"

#include "Engine/TextureRenderTarget2D.h"

// Sets default values
AGameMapPlane::AGameMapPlane()
{
	PlaneComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GamePlane"));
	RootComponent = PlaneComponent;
}

// Called when the game starts or when spawned
void AGameMapPlane::BeginPlay()
{
	Super::BeginPlay();

	if (!PlaneComponent)
	{
		UE_LOG(LogTemp, Warning, TEXT("!PlaneComponent"));
		return;
	}

	auto Material = PlaneComponent->GetMaterial(0);
	if (!Material)
	{
		UE_LOG(LogTemp, Warning, TEXT("!Material"));
		return;
	}
	DynamicMaterial = UMaterialInstanceDynamic::Create(Material, this);

	PlaneComponent->SetMaterial(0, DynamicMaterial);

	CreateMatTexture({ 100, 100 });
}

void AGameMapPlane::CreateMatTexture(FIntPoint Size)
{
	// Creates Texture2D to store TextureRenderTarget content
	Texture = UTexture2D::CreateTransient(Size.X, Size.Y,
	                                      PF_B8G8R8A8);
#if WITH_EDITORONLY_DATA
	Texture->MipGenSettings = TMGS_NoMipmaps;
#endif
	Texture->SRGB = true;
	Texture->Filter = TextureFilter::TF_Nearest;


	DynamicMaterial->SetTextureParameterValue(TEXT("TextureColor"), Texture);
}

void AGameMapPlane::UpdateTexture(TArray<FColor> const& Data, FIntPoint Size)
{
	PROFILER_SCOPE_FUNCTION();

	Size.X = FMath::Clamp(Size.X, 1, 16000);
	Size.Y = FMath::Clamp(Size.Y, 1, 16000);

	if (Size.X != Texture->GetSizeX() || Size.Y != Texture->GetSizeY())
	{
		CreateMatTexture({ Size.X, Size.Y });
	}

	if (Data.Num() == 0 || Data.Num() != Size.X * Size.Y)
	{
		UE_LOG(LogTemp, Warning, TEXT("Data.Num() == 0 || Data.Num() != Size.X * Size.Y"));
		return;
	}

	if (Size.X != Texture->GetSizeX() || Size.Y != Texture->GetSizeY())
	{
		UE_LOG(LogTemp, Warning, TEXT("Size.X != Texture->GetSizeX() || Size.Y != Texture->GetSizeY()"));
		return;
	}
	// Lock and copies the data between the textures
	void* TextureData = Texture->GetPlatformData()->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
	int TextureDataSize = Data.Num() * 4;
	FMemory::Memcpy(TextureData, Data.GetData(), TextureDataSize);
	Texture->GetPlatformData()->Mips[0].BulkData.Unlock();
	// Apply Texture changes to GPU memory
	Texture->UpdateResource();
}
