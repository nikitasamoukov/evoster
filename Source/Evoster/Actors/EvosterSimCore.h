#pragma once

#include "CoreMinimal.h"
#include "Evoster/Core/SimCore.h"
#include "GameFramework/Actor.h"
#include "EvosterSimCore.generated.h"


class AGameMapPlane;

UCLASS()
class EVOSTER_API AEvosterSimCore : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AEvosterSimCore();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	AGameMapPlane* GameMapPlane;

	UPROPERTY(BlueprintReadOnly)
	USimCore* GameCore;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
