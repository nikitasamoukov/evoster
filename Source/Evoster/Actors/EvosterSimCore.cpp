#include "EvosterSimCore.h"

#include "GameMapPlane.h"
#include "Evoster/Core/SimOptions.h"

// Sets default values
AEvosterSimCore::AEvosterSimCore()
{
	GameCore = CreateDefaultSubobject<USimCore>(TEXT("MMM"));
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AEvosterSimCore::BeginPlay()
{
	Super::BeginPlay();

	//GameCore->Start(NewObject<USimOptions>(this));
}

// Called every frame
void AEvosterSimCore::Tick(float DeltaTime)
{
	PROFILER_SCOPE_FUNCTION();
	Super::Tick(DeltaTime);

	if (!GameMapPlane)
	{
		UE_LOG(LogTemp, Warning, TEXT("!GameMapPlane"));
		return;
	}

	auto Pair = GameCore->GetColors();

	GameCore->Update();

	GameMapPlane->UpdateTexture(*Pair.Key, Pair.Value);
}
