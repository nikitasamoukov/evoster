#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Materials/MaterialInstance.h"

#include "GameMapPlane.generated.h"

UCLASS()
class EVOSTER_API AGameMapPlane : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AGameMapPlane();

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PlaneComponent;

	UPROPERTY(VisibleAnywhere)
	UMaterialInstance* MaterialInstance;

	UPROPERTY(VisibleAnywhere)
	UTexture2D* Texture;

	UPROPERTY(VisibleAnywhere)
	UMaterialInstanceDynamic* DynamicMaterial;

	void UpdateTexture(TArray<FColor> const& Data, FIntPoint Size);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	void CreateMatTexture(FIntPoint Size);
};
